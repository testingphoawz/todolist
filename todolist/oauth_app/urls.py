from django.urls import path, include
from . import views

urlpatterns = [
	path('add/todo/', views.AddTodo.as_view(), name="add_todo"),
	path('delete/todo/', views.DeleteTodo.as_view(), name='delete_todo'),
	path('get/todolist/', views.GetTodoList.as_view(), name='get_todolist'),
	path('mark/todo/', views.MarkTodo.as_view(), name='mark_todo'),
]
