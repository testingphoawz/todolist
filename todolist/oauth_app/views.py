from rest_framework.views import APIView
from rest_framework.response import Response
from .models import *
import json
from .serializers import *


# Create your views here.
class AddTodo(APIView):

    @staticmethod
    def post(request):
        if 'name' in request.POST:
            try:
                name = request.POST.get('name')

                if name:
                    Todo.objects.create(name=name)
                    return Response({'message': 'success created'}, status=200)

            except Exception as e:
                print('AddTodo | ERROR: ' + str(e))

        return Response({'message': 'Bad Request'}, status=400)


class DeleteTodo(APIView):

    @staticmethod
    def post(request):
        if 'name' in request.POST:
            try:
                name = request.POST.get('name')

                if name:
                    todo = Todo.objects.get(name=name)
                    todo.is_active = False
                    todo.save()

                    return Response({'message': 'success deleted'}, status=200)

            except Exception as e:
                print('DeleteTodo | ERROR: ' + str(e))

        return Response({'message': 'Bad Request'}, status=400)


class GetTodoList(APIView):

    @staticmethod
    def get(request):
        if request.GET.get('all') == '1':
            try:
                todolist = Todo.objects.all()
                todo_serialized = TodoSerializer(todolist, many=True)
                return Response(todo_serialized.data, status=200)

            except Exception as e:
                print('GET GetTodoList | FETCH ERROR: ' + str(e))

        return Response({'message': 'Bad Request'}, status=400)


class MarkTodo(APIView):

    @staticmethod
    def post(request):
        if 'name' in request.POST:
            try:
                name = request.POST.get('name')

                if name:
                    todo = Todo.objects.get(name=name)
                    todo.mark_as_done = True
                    todo.save()

                    return Response({'message': 'success mark as done'}, status=200)

            except Exception as e:
                print('MarkTodo | ERROR: ' + str(e))

        return Response({'message': 'Bad Request'}, status=400)