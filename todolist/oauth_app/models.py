from django.db import models


# Create your models here.
class Todo(models.Model):
    name = models.CharField(max_length=200, null=True, blank=True, unique=True)
    is_active = models.BooleanField(default=True)
    mark_as_done = models.BooleanField(default=False)
